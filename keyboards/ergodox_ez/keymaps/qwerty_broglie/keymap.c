#include QMK_KEYBOARD_H
#include "version.h"
#include "config.h"

// Layer Declarations
#define BASE 0 // default layer
#define EXTR 1 // extra layer
#define PASS 2 // blue pass layer

/* SEND_STRING() & process_record_user -
 * https://docs.qmk.fm/#/feature_macros?
 *      id=the-new-way-send_string-process_record_user
 */
enum custom_keycodes {
    TRNS = DF(BASE),   // Variant TRNS
    DICE = SAFE_RANGE, // Random digit key
    PASS_ZERO,
    PASS_BSPC,
    PASS_K0,  PASS_K1, PASS_K2,  PASS_K3,  PASS_K4,  PASS_K5, PASS_K6, PASS_K7,
    PASS_K8,  PASS_K9,PASS_K10, PASS_K11, PASS_K12, PASS_K13,PASS_K14,PASS_K15,
    PASS_K16,PASS_K17,PASS_K18, PASS_K19, PASS_K20, PASS_K21,PASS_K22,PASS_K23,
    PASS_K24,PASS_K25,PASS_K26, PASS_K27, PASS_K28, PASS_K29,PASS_K30,PASS_K31,
    PASS_K32,PASS_K34,PASS_K35, PASS_K36, PASS_K37, PASS_K38,PASS_K39,PASS_K40,
    PASS_K41,PASS_K42,PASS_K43, PASS_K44, PASS_K45, PASS_K46,PASS_K47,PASS_K48,
    PASS_K49,PASS_K50,PASS_K51, PASS_K52, PASS_K53, PASS_K54,PASS_K55,PASS_K56,
    PASS_K57,PASS_K58,PASS_K59, PASS_K60, PASS_K61, PASS_K62,PASS_K63,PASS_K64,
    PASS_K65,PASS_K66,PASS_K67, PASS_K68, PASS_K69, PASS_K70,PASS_K71,PASS_K72,
    PASS_K73,PASS_K74,PASS_K78, PASS_K79
};

#define RETRY 3

#define N 6*sizeof(uint16_t)
#define ZERO_PASS (uint16_t []){PASS_ZERO, PASS_ZERO, PASS_ZERO, PASS_ZERO, PASS_ZERO, PASS_ZERO} 

#define BLUE_PASS (uint16_t []){PASS_K0, PASS_K1, PASS_K2, PASS_K3, PASS_K4, PASS_K5} 
#define RED_PASS "secret"

bool password_user(uint16_t keycode) {
    static uint16_t keycodes[] = ZERO_PASS;
    static uint16_t retry = RETRY;

    if (retry == 0) {
        return true;
    } else {
        if (keycode == PASS_BSPC) {
            memcpy(keycodes, ZERO_PASS, N);
            return false;
        } else {
            uint16_t *push = (uint16_t *)memchr(keycodes, PASS_ZERO, N);
            if (push == 0) {
                retry -= 1;
                memcpy(keycodes, ZERO_PASS, N);
                return true;
            } else {
                *push = keycode;
                if (memcmp((char *)keycodes, (char *)BLUE_PASS, N) == 0) { 
                    retry = RETRY;
                    memcpy(keycodes, ZERO_PASS, N);
                    SEND_STRING(RED_PASS);
                    return true;
                } else {
                    return false;
                }
            }
        }
    }
}

bool process_record_user(uint16_t keycode, keyrecord_t *record) {
  if (keycode >= PASS_K0 && keycode <= PASS_K79) {
      static bool _switch = false; 
      if (record->event.pressed) {
        _switch = password_user(keycode);
      } else if (_switch) {
          _switch = false;
          set_single_persistent_default_layer(BASE);
      }
  } else if (keycode == DICE) {
      if (record->event.pressed) {
        send_char((rand() % 10) + '0');
      }
  }
  return true;
}

const uint16_t PROGMEM keymaps[][MATRIX_ROWS][MATRIX_COLS] = {
/* Keymap 0: Basic layer
 *
 * ,--------------------------------------------------.           ,--------------------------------------------------.
 * | `~     |   1! |   2@ |   3# |   4$ |   5% |  6^  |           |  7&  |   8* |   9( |   0) |   -_ |   =+ |     \| |
 * |--------+------+------+------+------+-------------|           |------+------+------+------+------+------+--------|
 * | Tab    |   Q  |   W  |   E  |   R  |   T  | «◌̂   |           |  »◌̈  |   Y  |   U  |   I  |   O  |   P  | (EXTR) |
 * |--------+------+------+------+------+------|      |           |      |------+------+------+------+------+--------|
 * | LAlt   |   A  |   S  |   D  |   F  |   G  |------|           |------|   H  |   J  |   K  |   L  |  ;:  |   RAlt |
 * |--------+------+------+------+------+------| '"   |           |      |------+------+------+------+------+--------|
 * | LShft  |   Z  |   X  |   C  |   V  |   B  |      |           |      |   N  |   M  |  ,<  |  .>  |   ?/ |  RShft |
 * `--------+------+------+------+------+-------------'           `-------------+------+------+------+------+--------'
 *  | LCtrl | PLft | PDow | Pup  | PRgt |                                       | Left | Down |  Up  | Rigt | RCtrl |
 *  `-----------------------------------'                                       `-----------------------------------'
 *                                        ,-------------.       ,-------------.
 *                                        | LWin |  Esc |       | Inst | RWin |
 *                                 ,------|------|------|       |------|------|------.
 *                                 | Delt | Spce | +Al1 |       |(DICE)| Entr | Supr |
 *                                 |      |      |------|       |------|      |      |
 *                                 |      |      | -Al1 |       |(PASS)|      |      |
 *                                 `--------------------'       `--------------------'
 */
[BASE] = LAYOUT_ergodox(
  // left hand
  KC_GRV,   KC_1,   KC_2,   KC_3,   KC_4,  KC_5, KC_6,
  KC_TAB,   KC_Q,   KC_W,   KC_E,   KC_R,  KC_T, KC_LBRC,
  KC_LALT,  KC_A,   KC_S,   KC_D,   KC_F,  KC_G,
  KC_LSHIFT,KC_Z,   KC_X,   KC_C,   KC_V,  KC_B, KC_QUOT,
  KC_LCTL,KC_HOME,KC_PGDN,KC_PGUP,KC_END,
                                                 KC_LWIN,  KC_ESCAPE,
                                                           LALT(KC_PLUS),
                                        KC_BSPC, KC_SPACE, LALT(KC_MINUS),
  // right hand
        KC_7,    KC_8,   KC_9,   KC_0,   KC_MINS, KC_EQL,  KC_BSLS,
        KC_RBRC, KC_Y,   KC_U,   KC_I,   KC_O,    KC_P,    DF(EXTR),
                 KC_H,   KC_J,   KC_K,   KC_L,    KC_SCLN, KC_RALT,
        KC_TRNS, KC_N,   KC_M,   KC_COMM,KC_DOT,  KC_SLSH, KC_RSHIFT,
                 KC_LEFT,KC_DOWN,KC_UP,  KC_RIGHT,KC_RCTL,
KC_INSERT, KC_RWIN,
DICE,
DF(PASS), KC_ENTER, KC_DEL
),

/* Keymap 1: Extention layer
 *
 * ,--------------------------------------------------.           ,--------------------------------------------------.
 * |        |  F1  |  F2  |  F3  |  F4  |  F5  | F6   |           | F7   |  F8  |  F9  |  F10 |  F11 |  F12 |        |
 * |--------+------+------+------+------+-------------|           |------+------+------+------+------+------+--------|
 * |        |      |      |      |      |      |      |           |  ,   |   1  |   2  |   3  |      |      |        |
 * |--------+------+------+------+------+------|      |           |      |------+------+------+------+------+--------|
 * | LAlt   |      |      |      |      |      |------|           |------|   4  |   5  |   6  |      |      |   RAlt |
 * |--------+------+------+------+------+------|      |           |  .   |------+------+------+------+------+--------|
 * | LShft  |      |      |      |      |      |      |           |      |   7  |   8  |   9  |      |      |  RShft |
 * `--------+------+------+------+------+-------------'           `-------------+------+------+------+------+--------'
 *  | LCtrl |MousLf|MousUp|MousDo|MousRg|                                       |   0  | Delt |      |      | RCtrl |
 *  `-----------------------------------'                                       `-----------------------------------'
 *                                        ,-------------.       ,-------------.
 *                                        |MClick|Speed0|       |      |      |
 *                                 ,------|------|------|       |------|------|------.
 *                                 |LClick|RClick|Speed1|       |      |      |      |
 *                                 |      |      |------|       |------|      |      |
 *                                 |      |      |Speed2|       |      |      |      |
 *                                 `--------------------'       `--------------------'
 */
[EXTR] = LAYOUT_ergodox(
  // left hand
  TRNS,  KC_F1,   KC_F2,   KC_F3,   KC_F4,  KC_F5, KC_F6,
  KC_TAB,TRNS, TRNS, TRNS, TRNS,TRNS, TRNS,
  KC_LALT,TRNS, TRNS, TRNS, TRNS,TRNS,
  KC_LSHIFT,TRNS, TRNS, TRNS, TRNS,TRNS, TRNS,
  KC_LCTRL,KC_MS_L, KC_MS_D, KC_MS_U, KC_MS_R,
                                                KC_BTN3, KC_ACL0,
                                                         KC_ACL1,
                                       KC_BTN1, KC_BTN2, KC_ACL2,
  // right hand
        KC_F7,  KC_F8,KC_F9, KC_F10,  KC_F11, KC_F12,  TRNS,
        KC_COMM, KC_1, KC_2,   KC_3, TRNS,TRNS,  DF(BASE),
                 KC_4, KC_5,   KC_6, TRNS,TRNS,  KC_RALT,
        KC_DOT,  KC_7, KC_8,   KC_9, TRNS,TRNS,  KC_RSHIFT,
                       KC_0,KC_BSPC, TRNS,TRNS,  KC_RCTRL,
TRNS, TRNS,
TRNS,
TRNS, TRNS, TRNS
),
[PASS] = LAYOUT_ergodox(
  // left hand
   PASS_K0,  PASS_K1, PASS_K2,  PASS_K3,  PASS_K4,  PASS_K5, PASS_K6,
   PASS_K7,  PASS_K8, PASS_K9, PASS_K10, PASS_K11, PASS_K12, PASS_K13,
  PASS_K14, PASS_K15,PASS_K16, PASS_K17, PASS_K18, PASS_K19,
  PASS_K20, PASS_K21,PASS_K22, PASS_K23, PASS_K24, PASS_K25, PASS_K26,
  PASS_K27, PASS_K28,PASS_K29, PASS_K30, PASS_K31,
                                               PASS_K32, PASS_K34,
                                                         PASS_K35,
                                     PASS_K36, PASS_K37, PASS_K38,
  // right hand
  PASS_K39, PASS_K40,PASS_K41, PASS_K42, PASS_K43, PASS_K44, PASS_K45,
  PASS_K46, PASS_K47,PASS_K48, PASS_K49, PASS_K50, PASS_K51, DF(BASE),
            PASS_K53,PASS_K54, PASS_K55, PASS_K56, PASS_K57, PASS_K58,
  PASS_K59, PASS_K60,PASS_K61, PASS_K62, PASS_K63, PASS_K64, PASS_K65,
                     PASS_K66, PASS_K67, PASS_K68, PASS_K69, PASS_K70,
PASS_K71, PASS_K72,
PASS_K73,
PASS_K74, PASS_K78, PASS_BSPC
)
};
